using System;

namespace proyecto_final_20199146_master
{
   
    class Examen
    {
        private string[] empleados;
        private int[,] sueldos;
        private int[] sfinal;

        public void Cargar() 
        {
            empleados=new String[4];
            sueldos=new int[4,3];
            for(int f = 0; f < empleados.Length; f++)
            {
                Console.Write("Ingrese el empleado:");
                empleados[f]=Console.ReadLine();
                for(int c = 0; c < sueldos.GetLength(1); c++) 
                {
                    Console.Write("Ingrese sueldo:");
                    string linea;
                    linea = Console.ReadLine();
                    sueldos[f,c]=int.Parse(linea);
                }
            }
        }

        public void CalcularSumaSueldos()
        {
            sfinal = new int[4];
            for (int f = 0; f < sueldos.GetLength(0); f++)
            {
                int suma = 0;
                for (int c = 0; c < sueldos.GetLength(1); c++)
                {
                    suma = suma + sueldos[f,c];
                }
                sfinal[f] = suma;
            }
        }

        public void ImprimirTotalPagado() 
        {
            Console.WriteLine("Total de sueldos de empleado.");
            for(int f = 0; f < sfinal.Length; f++) 
            {
                Console.WriteLine(empleados[f]+" - "+sfinal[f]);
            }
        }

        public void EmpleadoMayorSueldo() 
        {
            int may=sfinal[0];
            string nom=empleados[0];
            for(int f = 0; f < sfinal.Length; f++) 
            {
                if (sfinal[f] > may) 
                {
                    may=sfinal[f];
                    nom=empleados[f];
                }
            }
            Console.WriteLine("El empleado con mayor sueldo es "+ nom + " que tiene un sueldo de "+may);
        }

        static void Main(string[] args)
        {
            Examen ma = new Examen();
            ma.Cargar();
            ma.CalcularSumaSueldos();
            ma.ImprimirTotalPagado();
            ma.EmpleadoMayorSueldo();
            Console.ReadKey();
        }
    }
}
